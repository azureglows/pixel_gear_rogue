import tcod as libtcod


def handle_keys(key):
    # Movement keys
    if key.vk == libtcod.KEY_UP or key.text == 'w':
        return {'move': (0, -1)}
    elif key.vk == libtcod.KEY_DOWN or key.text == 's':
        return {'move': (0, 1)}
    elif key.vk == libtcod.KEY_LEFT or key.text == 'a':
        return {'move': (-1, 0)}
    elif key.vk == libtcod.KEY_RIGHT or key.text == 'd':
        return {'move': (1, 0)}

    if key.vk == libtcod.KEY_ENTER and key.lalt:
        # Alt+Enter: toggle full screen
        return {'fullscreen': True}
    elif key.vk == libtcod.KEY_ESCAPE:
        # Exit the game
        return {'exit': True}

    # There is no key
    return {}
