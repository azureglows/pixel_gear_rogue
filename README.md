# Pixel Gear Rogue
This is my first game, rogue-like or not. I intend this game to have some combat but rely on stealth as a core mechanic.
For the most part we are following along with the blow tutorial.  Some minor adjustments have been made to expand on ideas.
Below you can find some of the things I expanded on from the tutorial.

![example_map_image](docs/part_3_official_done.png "Procedural Generator Map")

#### Ideas expanded upon
* Found and used a different [font and font layout](http://lazyfoo.net/tutorials/OpenGL/20_bitmap_fonts/index.php) (part 1)
* Add wasd as movement keys along with arrow keys (part 3)

At [r/roguelikedev](https://www.reddit.com/r/roguelikedev/) we're doing a dev-along following [The Complete Roguelike Tutorial](http://rogueliketutorials.com/tutorials/tcod/)
## If you would like to participate on GitLab

* [Sign up for a free personal account](https://gitlab.com/users/sign_in#register-pane) if you don't already have one.
* Fork [this repository](https://gitlab.com/aaron-santos/roguelikedev-does-the-complete-roguelike-tutorial) to your account.
* Clone the repository on your computer and follow the tutorial.
* Follow along with the [weekly posts](https://www.reddit.com/r/roguelikedev).
* Update the `README.md` file to include a description of your game, how/where to play/download it, how to build/compile it, what dependencies it has, etc.
* Share your game on the final week.

## It's dangerous to go alone

If you're **new to Git, GitLab, or version control**…

* [Git Documentation](https://git-scm.com/documentation) - everything you need to know about version control, and how to get started with Git.
* [GitLab Help](https://gitlab.com/help/gitlab-basics/command-line-commands.md#start-working-on-your-project) - start working on your project.

## Requirements
> **_NOTE:_** LINUX (DEB BASED) ONLY: tcod has to be built from source so make sure packages in guide are installed
     https://python-tcod.readthedocs.io/en/latest/installation.html#linux-debian-based
* [pipenv](https://docs.pipenv.org/en/latest/install/)
    * This will install all python package dependencies by simply running `pipenv install`.
