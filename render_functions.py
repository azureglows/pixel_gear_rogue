import tcod as libtcod

from map_objects import game_map


def render_all(con, entities, game_map, fov_map, fov_recompute, screen_width, screen_height, colors):
    # Draw all the tiles in the game map
    draw_all_tiles(con, game_map, fov_map, fov_recompute, colors)

    # Draw all entities in the list
    for entity in entities:
        draw_entity(con, entity, colors, fov_map)

    libtcod.console_blit(con, 0, 0, screen_width, screen_height, 0, 0, 0)


def clear_all(con, entities, colors, fov_map):
    for entity in entities:
        if libtcod.map_is_in_fov(fov_map, entity.x, entity.y): clear_entity(con, entity, colors)


def draw_entity(con, entity, colors, fov_map):
    if libtcod.map_is_in_fov(fov_map, entity.x, entity.y):
        libtcod.console_put_char_ex(con, entity.x, entity.y, entity.char, colors.get('dark_ground'), entity.color)


def clear_entity(con, entity, colors):
    # erase the character that represents the object
    libtcod.console_put_char_ex(con, entity.x, entity.y, ' ', libtcod.black, colors.get('dark_ground'))


def draw_all_tiles(con, game_map, fov_map, fov_recompute, colors):
    if fov_recompute:
        for y in range(game_map.height):
            for x in range(game_map.width):
                visible = libtcod.map_is_in_fov(fov_map, x, y)
                wall = game_map.tiles[x][y].block_sight

                if visible:
                    if wall:
                        libtcod.console_set_char_background(con, x, y, colors.get('light_wall'), libtcod.BKGND_SET)
                    else:
                        libtcod.console_set_char_background(con, x, y, colors.get('light_ground'), libtcod.BKGND_SET)

                    game_map.tiles[x][y].explored = True
                elif game_map.tiles[x][y].explored:
                    if wall:
                        libtcod.console_set_char_background(con, x, y, colors.get('dark_wall'), libtcod.BKGND_SET)
                    else:
                        libtcod.console_set_char_background(con, x, y, colors.get('dark_ground'), libtcod.BKGND_SET)
